import React from 'react';

const Budget = () => (
  <iframe
    width="100%"
    title="IEEE uOttawa Budget"
    src="https://docs.google.com/spreadsheets/d/e/2PACX-1vT2PIGHy-IYbrOnad9BOQh-eGRwY5ogl3vKgtysYeJ7BZnzUDjI7wzJ_WvHHz7UysT0EEFbHDaZ6xR6/pubhtml?widget=true&amp;headers=false"
    style={{ height: '981px' }}
  />
);

export default Budget;

