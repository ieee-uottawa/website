export const NAV_ITEMS_QUERY = `
query GetNavItems {
  title
  link
  items
}
`;
